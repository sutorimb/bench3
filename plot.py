import lcm
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys
sys.path.append('./lcmtypes')
from ctrlpanel import pidData_t
plt.ion()
fig=plt.figure()
setpointlist=[]
inputlist=[]
timelist=[]
def plot_handler(channel,data):
	msg=pidData_t.decode(data)
	print("Received message on channel \"%s\"" % channel)
	
	timelist.append(msg.timestamp)
	setpointlist.append(msg.pidSetpoint)
	inputlist.append(msg.pidInput)
	fig.canvas.draw()
	
lc=lcm.LCM()
subscription=lc.subscribe("PIDDATA",plot_handler)

try:
	while True:
		lc.handle()
		plt.plot(timelist,setpointlist,'g')
		plt.plot(timelist,inputlist,'b')
		
except KeyboardInterrupt:
	np.savetxt("Time.csv",timelist)
	np.savetxt("Input.csv",setpointlist)
	np.savetxt("Output.csv",inputlist)
	
	
	
	
lc.unsubscribe(subscription)
