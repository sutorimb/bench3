/*********************
*   PID.c
*   simple pid library 
*   pgaskell
**********************/

#include "PID.h"
#include <stdio.h>
#include <stdlib.h>


#define MAX_OUTPUT 1.0
#define MIN_OUTPUT -1.0
#define ITERM_MIN -1.0
#define ITERM_MAX 1.0

PID_t * PID_Init(float Kp, float Ki, float Kd) {
  // allocate memory for PID
  PID_t *pid =  malloc(sizeof(PID_t));

  //initalize values
  pid->pidInput = 0;
  pid->pidOutput = 0;
  pid->pidSetpoint = 0;
  pid->ITerm = 0;
  pid->prevInput = 0;

  // set update rate to 100000 us (0.1s)
  PID_SetUpdateRate(pid, 100000);
  
  //set output limits, integral limits, tunings
  printf("initializing pid...\r\n");
  PID_SetOutputLimits(pid,MIN_OUTPUT,MAX_OUTPUT);
  PID_SetIntegralLimits(pid,ITERM_MIN,ITERM_MAX);
  PID_SetTunings(pid,Kp,Ki,Kd);
  return pid;
}

void PID_Compute(PID_t* pid) {
  float err=pid->pidSetpoint-pid->pidInput;
  float prop=err*pid->kp;
  float deriv=pid->kd*(err-pid->prevInput);
  pid->ITerm=(pid->ITerm+pid->ki*err);
  if (pid->ITerm>ITERM_MAX){
  	  pid->ITerm=ITERM_MAX;
  }
  else if (pid->ITerm<ITERM_MIN){
  	  pid->ITerm=ITERM_MIN;
  }
  pid->prevInput=err;
  if(pid->ki == 0){
  	pid->ITerm = 0;
  }
  float integral=pid->ITerm;
  pid->pidOutput=integral+deriv+prop;
  if (pid->pidOutput>MAX_OUTPUT){
      pid->pidOutput=MAX_OUTPUT;
  }
  else if (pid->pidOutput<MIN_OUTPUT){
      pid->pidOutput=MIN_OUTPUT;
  }else{
  }
  printf("Prop:%2f Integral:%2f Derivative:%2f \r\n",prop,integral,deriv);
}

void PID_SetTunings(PID_t* pid, float Kp, float Ki, float Kd) {
  //scale gains by update rate in seconds for proper units
  float updateRateInSec = ((float) (pid->updateRate / 1000000.0));
  //set gains in PID struct
  pid->kp=Kp;
  pid->ki=Ki*updateRateInSec;
  pid->kd=Kd/updateRateInSec;
}

void PID_SetOutputLimits(PID_t* pid, float min, float max){
  //set output limits in PID struct
  pid->outputMin = min;
  pid->outputMax = max;
}

void PID_SetIntegralLimits(PID_t* pid, float min, float max){
  //set integral limits in PID struct
  pid->ITermMin = min;
  pid->ITermMax = max;

}

void PID_SetUpdateRate(PID_t* pid, int updateRate){
  //set integral limits in PID struct
  pid->updateRate = updateRate;
}




