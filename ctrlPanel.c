/**************************************************************
*
*	CtrlPanel Test
*
***************************************************************/

#include "./libBBB/libBBB.h"
#include <math.h>
#include "PID.h"

/* LIBRARIES NEEDED FOR LCM*/
#include <inttypes.h>
#include <lcm/lcm.h>
#include "lcmtypes/ctrlPanelData_t.h"  // ctrlPanel lcm type
#include "lcmtypes/pidData_t.h"  // PID lcm type

//Sensor Limit calibration
//MEASURE FOR YOUR PANEL
#define F1MIN 43
#define F1MAX 1763
#define F2MIN 41
#define F2MAX 1753
#define P1MIN 0
#define P1MAX 1767
#define P2MIN 0
#define P2MAX 1767
#define P3MIN 1
#define P3MAX 1770

//set range of pot control
//YOU NEED TO DEFINE THESE
#define KPMAX 10
#define KIMAX 50
#define KDMAX 0.03

#define PI 3.14159265

struct Ctrlpanel_data{
  	int fader1;
  	int fader2;
  	int pots[3];
	int switches[3];
  	int motorSignal;
	float fader1_scaled;
	float fader2_scaled;
	float pots_scaled[3];
};

void InitializePanel(void){

 	/* Initiate Pin 66 Linked to Switch 1 */
        initPin(66);
        setPinDirection(66, INPUT);
              
        initPin(67);
        setPinDirection(67, INPUT);
        
        initPin(69);
        setPinDirection(69, INPUT);
        
        /* Initiate Pin 45 that defines motor direction */
        initPin(45);
        setPinDirection(45, OUTPUT);
	
        /* Initiate A/D Converter */
        initADC();

        /* Initiate PWM for Servo 1 */
        initPWM(SV1);
        setPWMPeriod(SV1H, SV1, 100000);
        setPWMPolarity(SV1H, SV1, 0);
        setPWMDuty(SV1H, SV1, 2000);
        setPWMOnOff(SV1H, SV1, ON);

}

/*******************************************    
*       Scaling functions to scale AD readings
*       0 to 1 for pots and -1 to 1 for faders
*******************************************/

float ScaleInt2PosFloat(float input, int min, int max){
    //scale readings to between 0 and 1
	return 	(1.0*input)/(1.0*(max - min));

}

float ScaleInt2Float(float input, int min, int max){
    //scale readings to between -1 and 1
    //IMPLEMENT ME!
	return 	((2.0*input)/(1.0*(max - min)))-1;
}

/*******************************************    
*       Read values of the sensors
*       IMPLEMENT THIS FIRST TO TEST 
*******************************************/

void ReadPanelValues(struct Ctrlpanel_data * ctrlpanel){

	/* Read ADCs and give time after each reading to settle */
	ctrlpanel->fader1 = readADC(HELPNUM, A0);
	usleep(100);
	ctrlpanel->fader2 = readADC(HELPNUM, A1);
	usleep(100);
	ctrlpanel->pots[0] = readADC(HELPNUM, A2);
	usleep(100);
    //ADD OTHER POTENTIOMETERS
 	ctrlpanel->pots[1] = readADC(HELPNUM, A3);
	usleep(100);
	ctrlpanel->pots[2] = readADC(HELPNUM, A4);
	usleep(100);

	/* Scale the readings */
  
	ctrlpanel->fader1_scaled = ScaleInt2Float(ctrlpanel->fader1,F1MIN, F1MAX);	
	ctrlpanel->fader2_scaled = ScaleInt2Float(ctrlpanel->fader2,F1MIN, F1MAX);	
	
	ctrlpanel->pots_scaled[0] = ScaleInt2PosFloat(ctrlpanel->pots[0],P1MIN, P1MAX);
	ctrlpanel->pots_scaled[1] = ScaleInt2PosFloat(ctrlpanel->pots[1],P2MIN, P2MAX);
	ctrlpanel->pots_scaled[2] = ScaleInt2PosFloat(ctrlpanel->pots[2],P3MIN, P3MAX);
   

	/* Read Switches position */
	ctrlpanel->switches[0] = getPinValue(66);
    ctrlpanel->switches[1] = getPinValue(67);
    ctrlpanel->switches[2] = getPinValue(69);
}


void PrintPanelValues(struct Ctrlpanel_data * ctrlpanel){

	// CHANGE TO PRINT ALL INPUTS AND EVENTUAL OUTPUTS YOU ALSO WANT TO CHECK
	printf("fader1: %d fader2: %d pot1: %d pot2: %d pot3: %d sw1: %d sw2: %d sw3: %d\r\n",ctrlpanel->fader1,ctrlpanel->fader2,ctrlpanel->pots[0],
	ctrlpanel->pots[1],ctrlpanel->pots[2],ctrlpanel->switches[0],ctrlpanel->switches[1],ctrlpanel->switches[2]);

}

/*******************************************    
*       
*       CONTROLLERS
*
*******************************************/
void SimpleFaderDriver(struct Ctrlpanel_data * ctrlpanel){
	
	/* Drive Fader1 motor with Fader 2 position */
	/* Position of Fader 2 is reflected in the speed of Fader 1 */
	/* Fader 2 in middle = Fader 1 does not move */
	/* Fader 2 left = Fader 1 moves to left up to limit */
	/* Fader 2 right = Fader 1 moves to right up to limit */
	  
	ctrlpanel->motorSignal = 200*(ctrlpanel->fader2-1700/2);
    setPinValue(45, ctrlpanel->motorSignal > 0);
    setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));

}

void BangBang(struct Ctrlpanel_data * ctrlpanel){
	ctrlpanel->motorSignal = 5000;
	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));

	if (ctrlpanel->fader2 > ctrlpanel->fader1){
		setPinValue(45, 1);
		printf("Moving left\n");
	}
	else if (ctrlpanel->fader2< ctrlpanel->fader1){
		setPinValue(45, 0);
		printf("Moving right\n");
	}
	else{
		setPWMDuty(SV1H, SV1,0);
	}
}

void PID(PID_t *pid, struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
  	if(ctrlpanel->switches[0]){
  		ctrlpanel->pots_scaled[0] = 0;
  	}
  	if(ctrlpanel->switches[1]){
  		ctrlpanel->pots_scaled[1] = 0;
  	}
  	if(ctrlpanel->switches[2]){
  		ctrlpanel->pots_scaled[2] = 0;
  	}
  	//pid->pidSetpoint = ctrlpanel->fader2_scaled;
  	/*if(utime_now()%10000000 > 4999999){
  		pid->pidSetpoint = -0.5;
  	}else{
  		pid->pidSetpoint = 0.5;
  	}*/
  	//(((float)utime_now()%1000000)/1000000.0));
  	pid->pidInput = ctrlpanel->fader1_scaled;
  	PID_SetTunings(pid,ctrlpanel->pots_scaled[0]*KPMAX,ctrlpanel->pots_scaled[1]*KIMAX,ctrlpanel->pots_scaled[2]*KDMAX);
    PID_Compute(pid);
    if(pid->pidOutput>0){
    	setPinValue(45, 1);
    }else{
    	setPinValue(45, 0);
    }
    ctrlpanel->motorSignal = abs(pid->pidOutput*100000);
    setPWMDuty(SV1H, SV1,ctrlpanel->motorSignal);
}


/*******************************************    
*       
*       LCM Message Preparation
*
*******************************************/

void CtrlPanelDataLCM(struct Ctrlpanel_data * ctrlpanel, const char *channel, lcm_t * lcm){

        ctrlPanelData_t msg = {};
        
	    /* Prepare message to send over lcm channel*/
        msg.timestamp = utime_now();
        msg.fader1 = ctrlpanel->fader1;
        msg.fader2 = ctrlpanel->fader2;
        msg.pots[0] = ctrlpanel->pots[0];
        msg.pots[1] = ctrlpanel->pots[1];
        msg.pots[2] = ctrlpanel->pots[2];
        msg.switches[0] = ctrlpanel->switches[0];
        msg.switches[1] = ctrlpanel->switches[1];
        msg.switches[2] = ctrlpanel->switches[2];

        ctrlPanelData_t_publish(lcm, channel, &msg);
}

void PIDLCM(struct Ctrlpanel_data * ctrlpanel, PID_t *pid, const char *channel, lcm_t * lcm){
	
	pidData_t msg = {};
	msg.timestamp = utime_now();
	msg.pidInput = pid->pidInput;
	msg.pidSetpoint = pid->pidSetpoint;
	msg.pidOutput = pid->pidOutput;
	msg.Kp = pid->kp;
	msg.Ki = pid->ki;
	msg.Kd = pid->kd;
	pidData_t_publish(lcm,channel,&msg);
    /* Prepare message to send over lcm channel*/
    //IMPLEMENT ME!
}



/*******************************************    
*       
*       MAIN FUNCTION
* 
*******************************************/
int main()
{

	struct Ctrlpanel_data ctrlpanel;
        
	// For Controllers
	int64_t thisTime, lastTime, timeDiff;
	int updateRate = 10000;
	// PID
	PID_t *pid;
    pid = PID_Init(0,0,0);
	PID_SetUpdateRate(pid,updateRate);
        // LCM
        const char *channel1 = "CTRL_PANEL_DATA";   // LCM channel to publish messages
        const char *channel2 = "PIDDATA";   // LCM channel to publish messages
        lcm_t * lcm = lcm_create(NULL);
        if(!lcm) {
                return 1;
                }

	// Panel Initialization
	InitializePanel();

	float Freq=2;
	int k=0;
	/* Never ending loop */	
	while(1)
	{
		ReadPanelValues(&ctrlpanel);

		// Control at a specified frequency
		thisTime = utime_now();
        timeDiff = thisTime - lastTime;
        if(timeDiff >= updateRate){
    	    lastTime = thisTime;
    	    //}
    	/*if (k%20>9){
    	pid->pidSetpoint = -0.5;
    	}
    	else{
    	pid->pidSetpoint=0.5;
   		}
   		k++;*/
  			//pid->pidSetpoint = 0.5*sin(2*PI*Freq*thisTime/1000000);
			//SimpleFaderDriver(&ctrlpanel);
			//BangBang(&ctrlpanel);
			PID(pid,&ctrlpanel);
			PIDLCM(&ctrlpanel,pid,channel2,lcm);
			//PrintPanelValues(&ctrlpanel);
			
		}
		//CtrlPanelDataLCM(&ctrlpanel,channel1,lcm);	
		
			
		
		// UNCOMMENT TO INCLUDE LCM MESSAGE
			
	
	}

	return 0;
}
